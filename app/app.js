var querystring = require('querystring');
const Crisp = require('node-crisp-api');
const request = require('request');
const utf8 = require('utf8');

//  CONFIG
const identifier = process.env['crisp_identifier'];
const key = process.env['crisp_key'];
const websiteId = process.env['crisp_websiteId'];
const authorization = new Buffer(identifier + ':' + key).toString('base64');
const user_id = process.env['bato_username'];
const password = process.env['bato_password'];
const confidence = parseInt(process.env['bato_confidence']);
const baseUrl = process.env['bato_nlu_host'];

var CrispClient = new Crisp();
CrispClient.authenticate(identifier, key);
CrispClient.on('message:send', function(msg) {
	try {
		const fingerprint = msg.fingerprint;
		if (msg.type == 'text') {
			var form = {
				user_id: user_id,
				password: password,
			};
			var formData = querystring.stringify(form);
			var contentLength = formData.length;
			request(
				{
					headers: {
						'Content-Length': contentLength,
						'Content-Type': 'application/x-www-form-urlencoded',
					},
					uri: baseUrl + '/api/v1/sign_in',
					form: formData,
					method: 'POST',
				},
				function(err, res, body) {
					request(
						{
							headers: {
								authorization: 'Bearer ' + JSON.parse(body).access_token,
							},
							uri:
								baseUrl +
								'/api/v1/interpret?query=' +
								utf8.encode(msg.content) +
								'&client_id=dashboard',
							method: 'GET',
						},
						function(error, response, body) {
							//NLU conditions
							if (JSON.parse(body).intent.confidence > confidence) {
								request(
									{
										headers: {
											Authorization: 'Basic ' + authorization,
											'Content-Type': 'application/json',
										},
										uri:
											'https://api.crisp.chat/v1/website/' +
											websiteId +
											'/conversation/' +
											msg.session_id +
											'/read',
										method: 'PATCH',
										json: {
											from: 'user',
											origin: 'chat',
											fingerprints: [fingerprint],
										},
									},
									function(error, response, body) {
										console.log(body);
									}
								);
								CrispClient.websiteConversations
									.sendMessage(websiteId, msg.session_id, {
										type: 'text',
										content: JSON.parse(body).answer,
										from: 'operator', //or user
										origin: 'chat',
									})
									.then(function(a) {})
									.catch(error => {
										console.log('crisp error', error);
									});
							}
						}
					);
				}
			);
		}
	} catch (e) {
		console.log('error', e);
	}
});
