# Bato Crisp Connector

This app connects your [Crisp](https://www.crisp.chat) acccount to [Bato](http://www.bato.ai) Inteligent Business Assistant.
If you like to empower your Crips with Bato chatbot, contact [info@bato.ai](http://bato.ai/contact/) to register for a Bato account.

## Bato Intelligent Assistant

Bato helps individuals and corporates to save more time and money just by facilitating user experience.
Bato is an intelligent chatbot that help you as a person to do your routines better and as a corporate to response your users'
questions more quickly and accurately.

## First Steps

1. Register for a _Bato_ account (contact [info@bato.ai](http://bato.ai/contact/)).
2. Register for a _Crisp_ account (sign up in [crisp.chat](http://crisp.chat)).
3. Get your _Crisp_ identifier and key from [here](https://go.crisp.chat/account/token/).
4. Get your _Crisp_ website ID from [here](https://app.crisp.chat/settings/websites).

## Docker Installation

1. Install Docker (for windows follow this [link](https://docs.docker.com/docker-for-windows/install/))

```
$ sudo apt-get install docker.io
```

2. Clone from Git.

```
$ git clone https://gitlab.com/business-bato/bato-crisp-connector.git
```

3. Edit docker-compose.yml with your keys For example:

```
environment:
    - crisp_identifier=441bb556-eb14-4406-a28e-93e109669897
    - crisp_key=05e04d98c5fda6a9ae0e01a1e596e8273faea3f24f18df9b9ed7438469ec3047
    - crisp_websiteId=8103bea3-c190-4dd8-9163-6fe7d507784e
    - bato_username=username
    - bato_password=123456
```

4. Run project.

```
$ sudo docker-compose up -d
```

## Manual Installation

1. Install Node.js (for windows follow this [link](https://nodejs.org/en/download/))

```
$ sudo apt install nodejs
```

2. Clone from Git.

```
$ git clone https://gitlab.com/business-bato/bato-crisp-connector.git
```

3. Install packages.

```
$ cd bato-crisp-connector/app && npm i
```

4. Run Node.js with passing you kyes For example:

```
           crisp_identifier=441bb556-eb14-4406-a28e-93ec9g1q9897 crisp_key=05e04d98c5fd6d9g5g0e01a1e596e8273faea3f24f18df9b9ed7438469ec3047 crisp_websiteId=8103b7a3-c190-4dd8-ac7b-6fe7d507784e bato_username=username bato_password=123456 confidence=0.7 NLU_URI=http://business.bato.ai:4001 node app.js
```
